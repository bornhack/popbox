include <lasercut/lasercut.scad>; 

echo( "material geometry");
thickness = 4;
U = 1.75*25.4; // 1 rack unit
echo( thickness=thickness );
echo( U=U);

echo( "zipties hole sizes" );
b_hole_height = 2; // binder holes
b_hole_width = 10;
echo( b_hole_height = b_hole_height);
echo( b_hole_width = b_hole_width);

echo( "other values" );
notch_length = 10; // length of outside part of merge corner
c_width = 4*thickness;
echo( notch_length=notch_length );
echo( c_width = c_width);

echo( "internal sizes");
int_width = 3/4*300-2*notch_length-2*thickness;
int_depth = 3/4*    225-2*thickness;
//int_depth=17.3*25.4;
int_height = 3*U; //4U high
echo( int_width = int_width );
echo( int_depth = int_depth );
echo( int_height = int_height );

echo( "calculated sizes");
sides_h = int_height+2*thickness+notch_length;
front_w = int_width+2*thickness+2*notch_length;
sides_w = int_depth+2*thickness+2*notch_length;
echo( sides_h = sides_h);
echo( front_w = front_w);
echo( sides_w = sides_w);

echo( "Display vars");
explode = 0;
echo( explode = explode );


// output flags
do_flatten = true;     // if true, make box flat for lasercutting, if false show 3D box

module draw_flat() {

    translate( [notch_length, notch_length,0] ) {
        bottom( int_width+2*thickness, int_depth+2*thickness);
    } 

    translate( [notch_length, sides_w+sides_h+2*30,0] ) {
        top( int_width+2*thickness, int_depth+2*thickness);
    } 

    // front
    translate([0,-30,0]) rotate( [180,0,0] )  {
        front_back( front_w, sides_h);
    }

    //back
    translate([0, int_depth+2*notch_length+2*thickness+30, 0])   {
        front_back( front_w, sides_h);
    }

    //left
    translate([-30, 0, 0]) rotate( [0,180,0] ) {
        side_l(sides_h, sides_w);
    }

    //right
    translate([2*notch_length+2*thickness+int_width+30, 0, 0])   {
        side_r(sides_h, sides_w);
    }
        
}

module round_rectangle( he, wi, th ) {
    translate( [he/2+wi-he, he/2, 0] ) cylinder( h=th, d=he, center=false );
    translate( [he/2      , he/2, 0] ) cylinder( h=th, d=he, center=false );
    translate( [he/2,          0, 0] ) cube( size=[wi-he, he, th], center=false );
}


module top( bw, bd) {
  t = thickness; nl = notch_length;
  points = [[0, 0], [0, bd], [bw, bd], [bw, 0], [0, 0]];

  n_front_l = [0, 0, nl+t, t];
  n_front_r = [bw-nl-t, 0, nl+t, t];
  n_front_c = [nl+t+int_width/3, 0, int_width/3-2*nl, t];
  n_back_l = [0, bd-t, nl+t, t];
  n_back_r = [bw-nl-t, bd-t, nl+t, t];
  n_back_c = [nl+t+int_width/3, bd-t, int_width/3-2*nl, t];

  n_side_l_l = [ 0, 0, t, nl+t];
  n_side_l_r = [ bw-t, 0, t, nl+t];
  n_side_l_c = [ 0, nl+t+int_depth/3, t, int_depth/3-2*nl];

  n_side_r_l = [ 0, bd-t-nl, t, nl+t];
  n_side_r_r = [ bw-t, bd-t-nl, t, nl+t];
  n_side_r_c = [ bw-t, nl+t+int_depth/3, t, int_depth/3-2*nl];

  lasercutout(thickness=t, points = points, 
               cutouts = [n_front_l, n_front_r, n_front_c,
                          n_back_l, n_back_r, n_back_c,
                          n_side_l_l, n_side_l_r, n_side_l_c,
                          n_side_r_l, n_side_r_r, n_side_r_c] );

}

module bottom( bw, bd) {
  t = thickness; nl = notch_length;
  points = [[0, 0], [0, bd], [bw, bd], [bw, 0], [0, 0]];

  c_sw = [0,0, c_width+t, t];
  c_se = [bw-c_width-t,0, c_width+t, t];
  c_nw = [0,bd-t, c_width+t, t];
  c_ne = [bw-c_width-t, bd-t, c_width+t, t];

  b_hole_l = [ t+b_hole_height, bd/2-b_hole_width/2,  
                    b_hole_height, b_hole_width];
  b_hole_r = [ bw-(t+b_hole_height), bd/2-b_hole_width/2,  
                    b_hole_height, b_hole_width];
    
  lasercutout(thickness=t, points = points,
                cutouts=[b_hole_l, b_hole_r,
                        c_sw, c_nw, c_se, c_ne]);   
 
}

module front_back(w,h) {
    t = thickness; nl = notch_length;
    points = [[0,0], [0,h], [ w, h], [w,0], [0,0]];
    
    bottom_slid=[notch_length+c_width+thickness, notch_length, int_width-2*c_width, thickness];
    merge_slid_l = [notch_length, h/2, thickness, h/2];
    merge_slid_r = [w-notch_length-thickness, h/2, thickness, h/2];
    b_hole_lA = [ notch_length-2*b_hole_height, h/2 , b_hole_height, b_hole_width];
    b_hole_lB = [ notch_length+thickness+1*b_hole_height, h/2 , b_hole_height, b_hole_width];
    b_hole_rA = [ w-(notch_length-2*b_hole_height+ b_hole_height), h/2 ,             b_hole_height, b_hole_width];
    b_hole_rB = [ w-(notch_length+thickness+ 2* b_hole_height), h/2 , b_hole_height,            b_hole_width];

    top_l = [2*nl+t, h-t, int_width/3, t];
    top_r = [w-(2*nl+t+int_width/3), h-t, int_width/3, t];
    lasercutout(thickness=thickness, 
                points = points,
                cutouts =[bottom_slid, merge_slid_l, merge_slid_r,
                        b_hole_lA, b_hole_lB,
                        b_hole_rA, b_hole_rB,
                        top_l, top_r]);  
}

module side_l( h,d ){
    difference() { 
        side(h, d );

        translate( [h*1/3, 0, -0.1*thickness] ) {
            translate( [0,   d/4, 0] ) {cylinder( h=thickness*1.2, r=h/6, center=false );}
            translate( [0, 3*d/4, 0] ) {cylinder( h=thickness*1.2, r=h/6, center=false );}
            translate( [-h/6, d/4, 0] ) cube( size=[h/3, d/2, thickness*1.2], center=false );
        }
    }
}

module side_r( h,d ){
   difference() { 
        side(h, d );

        translate( [5/6*h, d/8, -0.1*thickness] ) rotate( [0, 0, 90] ) {
            round_rectangle( h/6, 6/8*d, 1.2*thickness );
        }
    }
}

module side(h, d) {
    t = thickness; nl = notch_length;
    points = [[0,0], [h, 0], [h, d], [0,d], [0,0]];
    
    merge_slid_l=[0, notch_length, h/2, thickness];
    merge_slid_r=[0, d-notch_length-thickness, h/2, thickness];
    bottom_support_A = [0, notch_length+thickness, 
                        notch_length+thickness, int_depth];
    b_hole_lA = [ h/2, notch_length-2*b_hole_height, 
                b_hole_width, b_hole_height];
    b_hole_lB = [ h/2, notch_length+thickness+1*b_hole_height, 
                b_hole_width, b_hole_height];
    b_hole_rA = [ h/2, d-(notch_length-1*b_hole_height), 
                b_hole_width, b_hole_height];
    b_hole_rB = [ h/2, d-(notch_length+thickness+2*b_hole_height), 
                b_hole_width, b_hole_height];
    b_hole_bottom = [ notch_length+thickness+b_hole_height, d/2-b_hole_width/2,  
                b_hole_height, b_hole_width];

    top_l = [h-t, 2*nl+t, t, int_depth/3];
    top_r = [h-t, d-(2*nl+t+int_depth/3), t, int_depth/3];
    
    lasercutout(thickness=thickness, 
                points = points,
                cutouts =[merge_slid_l, merge_slid_r, bottom_support_A,
                        b_hole_lA, b_hole_lB, b_hole_rA, b_hole_rB, b_hole_bottom,
                        top_l, top_r]);  
}

module draw_3D() {
    translate( [notch_length, notch_length,notch_length] ) {
        bottom( int_width+2*thickness, int_depth+2*thickness);
    } 

    translate( [notch_length, notch_length,notch_length+int_height+thickness] ) {
        %top( int_width+2*thickness, int_depth+2*thickness);
    } 
    
    // front
    translate([0,notch_length+thickness-explode,0]) rotate( [90, 0,0] )  {
        front_back(int_width+2*thickness+2*notch_length, int_height+2*thickness+notch_length);
    }

    //back
    translate([0, int_depth+notch_length+thickness*2+explode, 0]) rotate( [90, 0,0] )   {
        front_back(int_width+2*thickness+2*notch_length, int_height+2*thickness+notch_length);
    }

    //left
    translate([notch_length+thickness-explode, 0, 0]) rotate( [0, -90,0] )   {
        %side_r(int_height+2*thickness+notch_length, int_depth+2*thickness+2*notch_length);
    }

    //right
    translate([notch_length+2*thickness+int_width+explode, 0, 0]) rotate( [0, -90,0] )   {
        side_l(int_height+2*thickness+notch_length, int_depth+2*thickness+2*notch_length);
    }
}

translate( [0,0, -thickness/2]) projection( cut=true ) draw_flat();
//draw_3D();
//side_r(int_height+2*thickness+notch_length, int_depth+2*thickness+2*notch_length);
//round_rectangle( 30, 100, 2 );