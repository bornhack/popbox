
include <boxes/box_BH19.scad>

include <lasercut/lasercut.scad>
include <design/popbox.scad>

echo_params();
projection( cut=true )
    draw_flat();
