// support modules.

// output and info
module echo_params() {

  echo( "material geometry");
  echo( thickness=thickness );
  echo( U=U);

  echo( "zipties hole sizes" );
  echo( b_hole_height = b_hole_height);
  echo( b_hole_width = b_hole_width);

  echo( "other values" );
  echo( notch_length=notch_length );
  echo( c_width = c_width);
  echo( fan_hole_r=fan_hole_r );
  echo( w_margin=w_margin );

  echo( "internal sizes");
  echo( int_width = int_width );
  echo( int_depth = int_depth );
  echo( int_height = int_height );

  echo( "calculated sizes");

  echo( sides_h = sides_h);
  echo( front_w = front_w);
  echo( sides_w = sides_w);
  echo( top_w = top_w);
  echo( top_d = top_d );

  echo( "Display vars");
  echo( explode = explode );
  echo( flat_d = flat_d);
  echo( lift_lid=lift_lid);
}




// draw 2 cirles+center square within the give height x width
module round_rectangle( he, wi, th ) {
    translate( [he/2+wi-he, he/2, 0] ) cylinder( h=th, d=he, center=false );
    translate( [he/2      , he/2, 0] ) cylinder( h=th, d=he, center=false );
    translate( [he/2,          0, 0] ) cube( size=[wi-he, he, th], center=false );
}

// draw hooks for setting
module combining_hook( thickness ){
    translate([0,0,0]) cube( size=[1.5*thickness, thickness, thickness], center=false );
    translate([thickness*1.5, thickness/2,0]) cylinder( h=thickness, d=thickness, center=false );
    translate([thickness, thickness/2,0]) cube( size=[thickness, 1.5*thickness, thickness], center=false );
    translate([1.5*thickness, 2*thickness,0]) cylinder( h=thickness, d=thickness, center=false );
}

// receiving end
// t_scale is a number > 1.0 to handle graphical display
module combining_hole( thickness, t_scale=1.2, extra_width=0.1 ){
    translate([-1*extra_width,0,-1*(t_scale-1.0)])
        cube( size=[thickness+2*extra_width, 2.5*thickness, t_scale*thickness], center=false );
}

module venting_holes( width, height, hole_size, thickness, t_scale=1.2 ) {

    difference() {
        translate([height/2, width/2]) cube( [height, width, thickness], center=true);
        venting_pattern(width, height, hole_size, thickness, t_scale);
    }
}
module venting_pattern(height, width, hole_size, thickness, t_scale=1.2) {
    line_height = sqrt(3)*hole_size;
    h_count = floor( (width-3*hole_size)/(2*hole_size))+1;
    v_count = floor( height/line_height);
    z_adjust = (t_scale-1.0)/2;

    vent_size_x = 2*hole_size*(h_count+1)-2*hole_size; // shady, but it works.
    vent_size_y = v_count*line_height;

    translate([(width-vent_size_x)/2,(height-vent_size_y)/2,0] ) {
    for( y = [0:v_count-1] ) {
        for( x = [0:h_count-1] ) {
            translate( [x*hole_size*2+(y%2)*hole_size,
                        line_height/2+y*line_height,
                        z_adjust ] )
                cylinder( d=hole_size, h=t_scale*thickness, center=true) ;
        }
        if( (y%2) == 0 ) {
                translate( [h_count*hole_size*2,
                            line_height/2+y*line_height,
                            z_adjust ] )
                    cylinder( d=hole_size, h=t_scale*thickness, center=true) ;
        }
    }
}
//}
}

// following https://www.3d-flightcases.co.uk/19-portable-server-racks/19-rack-dimensions.php
module rack_mount_holes( u_count, thickness ) {
    inch = 25.4;
    U = 1.75*inch; // 7/8 inch
    hole_size = 3/8*inch;

    width = 5/8*inch; //standard

    for( y = [0:u_count-1] ) {
        translate( [y*U+2/8*inch, width/2, 0] ) {
            cube( [hole_size, hole_size, thickness ], center=true );
            translate( [5/8*inch, 0, 0 ] )
                cube( [hole_size, hole_size, thickness ], center=true );
            translate( [10/8*inch, 0,0 ] )
                cube( [hole_size, hole_size, thickness ], center=true );

        }
    }


}

module support_test(){
    round_rectangle( 20, 100, 4 );
    translate([0, 100, 0])  combining_hook( 40 );
    translate([0, 200, 0])  combining_hole( 40, extra_width=10 );

    translate([0, 400, 0])  venting_pattern( 50, 100, 2, 2 );
    translate([0, 600, 0])  venting_holes( 50, 100, 2, 2 );

    translate([0, 800, 0]) {
        %square( size=[4*1.75*25.4, 5/8*25.4] );
        rack_mount_holes( 4, 5 );
    }
}

//support_test();
