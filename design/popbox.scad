//include <lasercut/lasercut.scad>;
//include <fan_holder_v2.scad>
include <support.scad>
include <calc_val.scad>

//w_margin = 1; // extra width for the inside

//test box
//thickness = 4;
//int_width = 200-4*thickness;
//int_depth = 6*inch; // 19" on the inside
//int_U_h = 2; // 4U high
//fan_hole_r=20; // size of disc in the back

// actual box
//w_margin = 1; // extra width for the inside
//thickness = 6;
//int_width = 650-4*thickness+2*w_margin;
//int_depth = 19*inch; // 19" on the inside
//int_U_h = 4; // 4U high
//fan_hole_r=40; // size of disc in the back


// output flags
//do_flatten = true;     // if true, make box flat for lasercutting, if false show 3D box

module top( bw, bd) {
  t = thickness;
  points = [[0, 0], [0, bd], [bw, bd], [bw, 0], [0, 0]];

  n_front_l = [0, 0, c_width+t, t];
  n_front_r = [bw-c_width-t, 0, c_width+t, t];
  n_front_c = [c_width+t+int_width/3, 0, int_width/3-2*c_width, t];
  n_back_l = [0, bd-t, c_width+t, t];
  n_back_r = [bw-c_width-t, bd-t, c_width+t, t];
  n_back_c = [c_width+t+int_width/3, bd-t, int_width/3-2*c_width, t];

  n_side_l_l = [ 0, 0, t, c_width+t];
  n_side_l_r = [ bw-t, 0, t, c_width+t];
  n_side_l_c = [ 0, c_width+t+int_depth/3, t, int_depth/3-2*c_width];

  n_side_r_l = [ 0, bd-t-c_width, t, c_width+t];
  n_side_r_r = [ bw-t, bd-t-c_width, t, c_width+t];
  n_side_r_c = [ bw-t, c_width+t+int_depth/3, t, int_depth/3-2*c_width];

  translate( [0,0, -1/2*thickness] )
    lasercutout(thickness=t, points = points,
               cutouts = [n_front_l, n_front_r, n_front_c,
                          n_back_l, n_back_r, n_back_c,
                          n_side_l_l, n_side_l_r, n_side_l_c,
                          n_side_r_l, n_side_r_r, n_side_r_c] );

}

module bottom( bw, bd) {
  t = thickness; nl = notch_length;
  points = [[0, 0], [0, bd], [bw, bd], [bw, 0], [0, 0]];

  c_sw = [0,0, c_width+t, t];
  c_se = [bw-c_width-t,0, c_width+t, t];
  c_nw = [0,bd-t, c_width+t, t];
  c_ne = [bw-c_width-t, bd-t, c_width+t, t];

//  c_s = [(bw-c_width)/2, 0, c_width+t, t];
  c_s = [(bw-c_width)/2, 0, c_width, t];
  c_n = [(bw-c_width)/2, bd-t, c_width, t];

  b_hole_l = [ t+b_hole_height, bd/2-b_hole_width/2,
                    b_hole_height, b_hole_width];
  b_hole_r = [ bw-t-2*b_hole_height, bd/2-b_hole_width/2,
                    b_hole_height, b_hole_width];

  b_hole_t = [ bw/2-b_hole_width/2, t+b_hole_height,
                    b_hole_width, b_hole_height];
  b_hole_b = [ bw/2-b_hole_width/2, bd-t-thickness,
                    b_hole_width, b_hole_height];



  translate( [0,0, -1/2*thickness] )

    difference () {
      lasercutout(thickness=t, points = points,
                    cutouts=[b_hole_l, b_hole_r,
                            c_sw, c_nw, c_se, c_ne,
                            c_s, c_n,
                            b_hole_t, b_hole_b]);

       translate( [thickness+front_gap, 0, 0] )
            rotate( [90, 0, 90] )
            dev_front( int_height+thickness, notch_length+thickness );

       translate( [thickness+front_gap, sides_w-2*thickness, ] )
            rotate( [90, 0, -90] )
            dev_front( int_height+thickness, notch_length+thickness );
    }
}

module front_back(full_w, full_h) {
    t = thickness; nl = notch_length;

    h=full_h; w=full_w-4*t;

    points = [[2*t,0], [2*t,h], [2*t+w, h], [2*t+w,0], [2*t,0]];

    top_l = [2*t+c_width, h-t, int_width/3, t];
    top_r = [2*t+w-c_width-int_width/3, h-t, int_width/3, t];

    b_hole_l = [ 2*t+b_hole_height, h/2 , b_hole_height, b_hole_width];
    b_hole_r = [ 2*t+w-2*b_hole_height, h/2, b_hole_height, b_hole_width];

    b_hole_b = [ full_w/2-b_hole_width/2, bottom_lift+thickness+b_hole_height,
                 b_hole_width, b_hole_height];
    b_hole_b2 = [ full_w/2-b_hole_width/2,
                  bottom_lift-2*b_hole_height,
                  b_hole_width, b_hole_height];

    translate( [0,0, -1/2*thickness ] )
    difference() {
        group() {
            lasercutout(thickness=thickness,
                        points = points,
                        cutouts =[ //bottom_slid,
                                b_hole_l, b_hole_r, b_hole_b, b_hole_b2,
                                top_l, top_r]);

            for( i = [0:3] )
            {
             translate( [2*t+w, full_h/5*(i+1), 0] )
                combining_hook( thickness );
             translate( [2*t, full_h/5*(i+1), thickness] )
                rotate( [0, 180, 0 ])
                combining_hook( thickness );
            }
        }


        // venting holes
        translate( [full_w-int_width/3-2*thickness-c_width,
                    sides_h-h/3-thickness,
                    0.5*thickness] )
            venting_pattern( int_height/3, int_width/3,
                              thickness*1.5, thickness );

        // rack mount support
        translate( [2*thickness+front_gap, bottom_lift, thickness] )
            rotate( [0, 90, 0] )
            dev_front( int_height+thickness, notch_length+thickness );

        // bottom slid
        translate( [thickness, bottom_lift+thickness/2, 0] )
            rotate( [90, 0,0 ] )
            bottom( top_w, top_d );

        // handle
        center_width = int_width/3-c_width*2;
        translate( [2*t+1*c_width+int_width/3+0.1*center_width,
                    sides_h-3*thickness-handle_height,
                    -0.1*thickness] )
            round_rectangle( handle_height,
                            center_width*0.8,
                            thickness*1.2);
    }
}

module side_l( h,d ){
// perpas we want to do this
//    fan_mount(size=80,thick = 1.2*thickness);

    difference() {
        side(h, d );

        translate( [bottom_lift+2*thickness,
                    1/10*sides_w, 0] )
            venting_pattern( 8/10*sides_w, int_height/3,
                              thickness*1.5, thickness );

        translate( [((2*thickness+bottom_lift+int_height/3)+(sides_h-2*thickness))/2,
                     0, 0] ) {
            translate( [0, sides_w/4, 0 ] )
                cylinder( r=fan_hole_r, h=thickness*1.2, center=true);
            translate( [0, 3*sides_w/4, 0 ] )
                cylinder( r=fan_hole_r, h=thickness*1.2, center=true);
      }
    }
}

module side_r( h,d ){
   difference() {
        side(h, d );

        translate( [5/6*h, d/8, -(1/2+0.1)*thickness] )
            rotate( [0, 0, 90] )
            round_rectangle( h/6, 6/8*d, 1.2*thickness );


        translate( [h/3+bottom_lift+3*thickness, d/8, 0] )
            rotate( [0, 0, 90] )
            venting_pattern( h/3, 6/8*d, thickness*1.5, thickness );
    }
  }

module side(h, d) {
    t = thickness;
    points = [[0,0], [h, 0], [h, d], [0,d], [0,0]];

    bottom_support_A = [0, 2*thickness,
                        bottom_lift+thickness, int_depth];

    b_hole_lA = [ h/2, thickness-2*b_hole_height,
                b_hole_width, b_hole_height];
    b_hole_lB = [ h/2, 2*thickness+b_hole_height,
                b_hole_width, b_hole_height];
    b_hole_rA = [ h/2, d-(thickness-b_hole_height),
                b_hole_width, b_hole_height];
    b_hole_rB = [ h/2, d-(2*thickness+2*b_hole_height),
                b_hole_width, b_hole_height];

    b_hole_bottom = [ bottom_lift+thickness+b_hole_height, d/2-b_hole_width/2,
                b_hole_height, b_hole_width];

    top_l = [h-t, c_width+2*t, t, int_depth/3];
    top_r = [h-t, d-(c_width+2*t+int_depth/3), t, int_depth/3];


    translate( [0,0, -1/2*thickness] )
        difference() {
            lasercutout(thickness=thickness,
                        points = points,
                        cutouts =[ bottom_support_A,
                                b_hole_lA, b_hole_lB,
                                b_hole_rA, b_hole_rB, b_hole_bottom,
                                top_l, top_r]);
            for( i = [0:3] )
            {
             translate( [h/5*(i+1)+1.5*thickness, thickness, 0] )
                rotate( [0,0,90])
                combining_hole( thickness, extra_width=0.1 );
             translate( [h/5*(i+1)+1.5*thickness, d-2*thickness, 0] )
                rotate( [0,0,90])
                combining_hole( thickness, extra_width=0.1 );
            }
        }
}

module dev_front( h, w ) {
      points = [[0,0], [w-thickness, 0],
                [w-thickness, h-thickness], [0,h-thickness],
                [0,0]];

      difference() {
          translate( [thickness,thickness,-1/2*thickness] )

                lasercutout(thickness=thickness,
                            points = points,
                            finger_joints=[ [DOWN, 1, 2], [LEFT, 0, 4]  ] );

            translate( [w, thickness, 0] )
                rotate( [0,0, 90] )
                rack_mount_holes( int_U_h, 1.2*thickness );
      }
}

module draw_3D() {
  sides_h = int_height+2*thickness+bottom_lift;
  front_w = int_width+2*thickness+2*thickness;
  sides_w = int_depth+2*thickness+2*thickness;

  top_w = int_width+2*thickness;
  top_d = int_depth+2*thickness;

    union() {
    translate( [thickness, thickness,bottom_lift+1/2*thickness] )
        bottom( top_w, top_d );

    translate( lift_lid )
        translate( [thickness, thickness,sides_h-1/2*thickness] )
        top( top_w, top_d );

    // front
    translate([0,2*thickness-1/2*thickness-explode,0])
        rotate( [90, 0,0] )
        front_back( front_w, sides_h);

    //back
    translate([0,sides_w-thickness-1/2*thickness+explode,0]) rotate( [90, 0,0] )
        front_back( front_w, sides_h);

    //left
    translate([2*thickness-1/2*thickness-explode, 0, 0]) rotate( [0, -90,0] )
        side_r(sides_h, sides_w);

    //right
    translate([front_w-thickness-1/2*thickness+explode, 0, 0])
        rotate( [0, -90,0] )
        side_l(sides_h, sides_w);

    translate([front_gap+2*thickness, thickness, bottom_lift])
        rotate( [90, 0, 90 ] )
        dev_front( int_height+thickness, notch_length+w_margin+thickness );

    translate([front_gap+2*thickness,
                sides_w-thickness,
                bottom_lift])
        rotate( [90, 0, -90 ] )
        dev_front( int_height+thickness, notch_length+w_margin+thickness );
    }
}

module draw_flat() {


    translate( [thickness, 0,0] )
        bottom( top_w, top_d );

    translate( [thickness, sides_w+sides_h+2*flat_d,0] )
        top( top_w, top_d );

    // front
    translate([0,-1*flat_d, 0])
        rotate( [180,0,0] )
        front_back( front_w, sides_h);

    //back
    translate([0, sides_w+flat_d, 0])
        front_back( front_w, sides_h);

    //left
    translate([-1*flat_d, 0, 0])
        rotate( [0,180,0] )
        side_l(sides_h, sides_w);

    //right
    translate([2*notch_length+2*thickness+int_width+flat_d, 0, 0])
        side_r(sides_h, sides_w);

    translate( [-1* notch_length-flat_d,-1*flat_d-bottom_lift, 0] )
        rotate( [180, 0, 0] )
        dev_front( int_height+thickness, notch_length+w_margin+thickness );

    translate( [-1* notch_length-flat_d, flat_d+bottom_lift+sides_w, 0] )
        rotate( [0, 0, 0] )
        dev_front( int_height+thickness, notch_length+w_margin+thickness );
}

// draw flat, include projection at the end
//projection( cut=true )
//    draw_flat();

// 3D output for visualization
//draw_3D();
