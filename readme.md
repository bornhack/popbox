This project contains the scad files for generating the POP boxes used at Bornhack.

How to use
=============

1. Create a new file in `boxes/` by copying an existing one
2. Change the relevant parameters
3. Run `bash gen_output.sh boxes/my_box.scad`
4. you now have

    * `my_box-3D.scad`: the 3D rendering for openscad
    * `my_box-flat.scad`: the flat rendering for openscad
    * `my_box.dxf`: the dxf file for laser cutters or whatever other tool you wish
    * `my_box.svg`: as the dxf file, but in svg since that is what our fablab uses
    * `my_box.stl`: the 3D rendering in stl for presentation


Please note
------------

* the stl file probably is "not a valid two-manifold". So it cannot be 3D printet.

    (pull request to fix this would be appreciated)

* I have had a lot of adobe illustrator vs. inkscape issues related to scaling. One uses 72 pixel per point and the other 90 - so I usually have to scale to 75%.

* added some page with html and stl/dxf visualization could be cool
