#!/bin/bash

if [ "x" == "x$1" ]; then
	echo "Usage: $0 <scad parameter file>"
	exit 1
fi


PARAM_FILE=$1

cat << EOF

include <$PARAM_FILE>

include <lasercut/lasercut.scad>
include <design/popbox.scad>


echo_params();
draw_3D();

EOF
