#!/bin/bash

if [ "x" == "x$1" ]; then
	echo "Usage: $0 <scad parameter file>"
	exit 1
fi
PFILE=$1

PFILE_DIR=boxes
OUTDIR=box_output
PWD=`pwd`

mkdir -p $OUTDIR

echo "Generating DXF and STL files based on parameter file $PFILE"
echo "Processing $PFILE"

BASENAME=$(basename $PFILE_DIR/$PFILE .scad)

PFILE3D="${BASENAME}-3D.scad"
echo "- generating $PFILE3D"
bash $PWD/gen_scad_3D.sh $PFILE > $PFILE3D

echo "- generating $OUTDIR/$BASENAME.stl"
openscad -o $PWD/$OUTDIR/$BASENAME.stl $PFILE3D

PFILE_FLAT="${BASENAME}-flat.scad"
echo "- generating $PFILE_FLAT"
bash $PWD/gen_scad_flat.sh $PFILE > $PFILE_FLAT

echo "- generating DXF"
openscad -o $PWD/$OUTDIR/$BASENAME.dxf $PFILE_FLAT

echo "- converting to svg"
inkscape -z $PWD/$OUTDIR/$BASENAME.dxf -l $PWD/$OUTDIR/$BASENAME.svg

echo "Files in $OUTDIR"
file $OUTDIR/*
