// basic stuff (don't touch)
inch = 25.4; // inch in mm
U = 1.75*inch; // 1 rack unit

//geometry
w_margin = 1; // extra width for the inside: 1 mm is good
thickness = 6; // plate thickness in mm

int_width = 650-4*thickness+2*w_margin;
int_depth = 19*inch; // 19" on the inside
int_U_h = 4; // 4U high on the inside
int_height = int_U_h*U; // 4U high converted to mm

fan_hole_r = 40; // size of disc in the back in mm
notch_length = 5/8*inch; // length of outside part of merge corner (5/8" is rack default)
c_width = 4*thickness; // corner width - cut out distance from corner top + bottom slid
bottom_lift = 2*thickness; // box lift height from ground
front_gap = 120; // internal distance to front
handle_height = 30; // mm - choose a height that fits fingers.

// binder holes
b_hole_height = 2; // binder hole height (mm)
b_hole_width = 10; // binder hole width (mm)

// view parameters only
flat_d = 30; // 2D distance between plates in view for dxf
lift_lid = [ 0, 0, 100];  // values for lifting the lid for stl
explode = 5*thickness; // 3D distance between elements for stl
